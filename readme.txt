# ReactJS.NET App #

An example app using **ReactJS.NET** [https://reactjs.net/](Link URL)

.NET MVC 5
React.Web.Mvc4.2.5.0 (for both MVC 4 & 5)

## Getting Started ##

[https://reactjs.net/getting-started/tutorial.html](Link URL)

Commits represent each step in tutorial above.

## Tutorial: Got Up To ##

https://reactjs.net/getting-started/tutorial.html#optimization-optimistic-updates

## Additional Reading ##

### 9 Things Every ReactJS Beginner Should Know ###
https://camjackson.net/post/9-things-every-reactjs-beginner-should-know

If you're coming from the MVC world, you need to realise that React is just the 'View', part of the equation. Not the 'Model' or 'Controller'
