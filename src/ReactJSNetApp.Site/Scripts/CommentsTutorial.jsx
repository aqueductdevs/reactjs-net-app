﻿var CommentList = React.createClass({
  render: function() {
    var commentNodes = this.props.data.map(function (comment) {
          return (
            <Comment key={comment.Id} author={comment.Author}>
            {comment.Text}
          </Comment>
        );
    });
    return (
      <div className="commentList">
        {commentNodes}
      </div>
    );
  }
});

var Comment = React.createClass({
    render: function () {
        var converter = new Showdown.converter();
        var rawMarkup = converter.makeHtml(this.props.children.toString());
        return (
          <div className="comment">
            <h2 className="commentAuthor">
              {this.props.author}
            </h2>
            <span dangerouslySetInnerHTML={{__html: rawMarkup}} />
          </div>
      );
    }
});

var CommentForm = React.createClass({

    handleSubmit: function (e) {
        e.preventDefault();
        var author = this.refs.author.value.trim();
        var text = this.refs.text.value.trim();
        if (!text || !author) {
            return;
        }

        // Send request to the server
        this.props.onCommentSubmit({ Author: author, Text: text });

        this.refs.author.value = '';
        this.refs.text.value = '';
        return;
    },

    render: function() {
        return (
            <form className="commentForm" onSubmit={this.handleSubmit}>
            <input type="text" placeholder="Your name" ref="author" />
            <input type="text" placeholder="Say something..." ref="text" />
            <input type="submit" value="Post" />
            </form>
        );
    }
});

var CommentBox = React.createClass({

    // Load JSON comments from server
    loadCommentsFromServer: function () {
        var xhr = new XMLHttpRequest();
        xhr.open('get', this.props.url, true);
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);
            this.setState({ data: data });
        }.bind(this);
        xhr.send();
    },

    // Submit comment event
    handleCommentSubmit: function (comment) {
        // Submit to the server and refresh the comments list
        var data = new FormData();
        data.append('Author', comment.Author);
        data.append('Text', comment.Text);

        var xhr = new XMLHttpRequest();
        xhr.open('post', this.props.submitUrl, true);
        xhr.onload = function () {
            this.loadCommentsFromServer();
        }.bind(this);
        xhr.send(data);
    },

    // Initialise component
    getInitialState: function () {
        return { data: [] };
    },

    // After component has rendered
    componentDidMount: function () {
        this.loadCommentsFromServer();
        window.setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    },

    //Render
    render: function() {
        return (
          <div className="commentBox">
            <h1>Comments</h1>
            <CommentList data={this.state.data} />
            <CommentForm onCommentSubmit={this.handleCommentSubmit} />
          </div>
      );
    }
});
ReactDOM.render(
  <CommentBox url="/comments" pollInterval={2000} submitUrl="/comments/new" />,
  document.getElementById('content')
);