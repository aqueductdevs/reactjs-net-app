﻿using System;

namespace ReactJSNetApp.Site.Models
{
    public class CommentModel
    {
        public Guid Id { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
    }
}